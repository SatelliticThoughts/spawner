extends Node2D


enum SpawnType {
	constant,
	random,
	time
}


export (SpawnType) var spawn_type = SpawnType.constant
export (String) var body_path
export (int) var max_body_counter
export (int) var random_rate
export (float) var time_delay = 1

var _body_counter = 0
var _delay: int = 3
var _empty: bool = true
var _on_screen = false
var _timer: Timer

func _ready() -> void:
	if spawn_type == SpawnType.time:
		_timer = Timer.new()
		_timer.set_wait_time(time_delay)
		_timer.connect("timeout", self, "add_body")
		add_child(_timer)
		_timer.start()

func _process(delta: float) -> void:
	if not _on_screen:
		if _body_counter < max_body_counter:
			if _empty:
				if _delay < 1:
					if spawn_type == SpawnType.constant:
						constant()
					elif spawn_type == SpawnType.random:
						random()
					_delay = 3
				else:
					_delay -= 1

func constant() -> void:
	add_body()
	
	
func random() -> void:
	randomize()
	
	var chance: int = randi() % random_rate + 1
	if chance == 1:
		add_body()
	
	
func add_body():
	var body: PhysicsBody2D = load(body_path).instance()
	add_child(body)
	_body_counter += 1
	

func _on_Area2D_body_entered(body: PhysicsBody2D) -> void:
	_empty = false


func _on_Area2D_body_exited(body: PhysicsBody2D) -> void:
	_empty = true


func _on_VisibilityNotifier2D_screen_entered():
	_on_screen = true
	if spawn_type == SpawnType.time:
		_timer.stop()


func _on_VisibilityNotifier2D_screen_exited():
	_on_screen = false
	if spawn_type == SpawnType.time:
		_timer.start()
