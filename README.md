# Spawner

A 2D PhysicsBody Spawner for godot.

## Usage

Select a Node type that inherits from a PhysicsBody\
Select a spawn type:

Spawn Type | Description
---------- | ------------
Constant   | Will continuously spawn once PhysicsBody2D Leaves spawn area
Random     | Will spawn a PhysicsBody2D every 1 in X chance per process call
Time       | Will continuously spawn after X seconds

## License

[MIT](https://choosealicense.com/licenses/mit/)

